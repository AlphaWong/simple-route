package routers

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/context/param"
)

func init() {

	beego.GlobalControllerRouter["gitlab.com/simple-route/controllers:HereController"] = append(beego.GlobalControllerRouter["gitlab.com/simple-route/controllers:HereController"],
		beego.ControllerComments{
			Method: "GetToken",
			Router: `/v1/route`,
			AllowHTTPMethods: []string{"post"},
			MethodParams: param.Make(),
			Params: nil})

	beego.GlobalControllerRouter["gitlab.com/simple-route/controllers:HereController"] = append(beego.GlobalControllerRouter["gitlab.com/simple-route/controllers:HereController"],
		beego.ControllerComments{
			Method: "GetShortestRoute",
			Router: `/v1/route/:token`,
			AllowHTTPMethods: []string{"get"},
			MethodParams: param.Make(),
			Params: nil})

}
