package utils

const BadRequestErrorMessage = "INVALID_JSON_FORMATE"
const RedisConnectionErrorMessage = "CANNOT_CONNECT_REDIS"

// TokenNotFoundErrorMessage means the token not found / expired
const TokenNotFoundErrorMessage = "TOKEN_NOT_FOUND"
