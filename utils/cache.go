package utils

import (
	"encoding/json"
	"flag"
	"fmt"
	"strings"
	"time"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/cache"
	_ "github.com/astaxie/beego/cache/redis"
	"github.com/garyburd/redigo/redis"

	"gitlab.com/simple-route/types"
)

const CacheTimeout = 60

var C cache.Cache

var redisURI = beego.AppConfig.String("redisURI")

func NewCacheConnection() (cache.Cache, error) {
	if nil == C {
		bm, err := cache.NewCache("redis", fmt.Sprintf(`{"conn":"%s","dbNum":"0"}`, redisURI))
		if nil != err {
			return nil, err
		}
		C = bm
	}
	return C, nil
}

func RedisToString(val interface{}) (string, error) {
	return redis.String(val, nil)
}

func SetResponseToCache(tokenString string, response *types.ShortestRouteResponse) error {
	c, err := NewCacheConnection()
	remoteResponse, _ := json.Marshal(response)
	c.Put(fmt.Sprintf(HereMapCacheKey, tokenString), string(remoteResponse), time.Duration(CacheTimeout)*time.Second)
	return err
}

func GetResponseFromCache(tokenString string) (response *types.ShortestRouteResponse, err error) {
	c, err := NewCacheConnection()
	p, err := RedisToString(c.Get(fmt.Sprintf(HereMapCacheKey, tokenString)))
	json.NewDecoder(strings.NewReader(p)).Decode(&response)
	return response, err
}

func InitRedis() {
	var host string
	flag.StringVar(&host, "host", "0.0.0.0", "host of redis")
	flag.Parse()
	redisURI = fmt.Sprintf(redisURI, host)
}
