package utils

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"net/http/httputil"
	"net/url"
	"strings"

	"gitlab.com/simple-route/types"
)

func GetShortestRoute(points ...[]string) (float64, float64, error) {
	var hereMapResp types.HereMapResponse
	v := PrepareShortestRoute(points...)
	u, err := url.Parse(HereMapURI)
	if nil != err {
		return 0, 0, err
	}
	u.RawQuery = v.Encode()
	resp, err := http.Get(u.String())
	if nil != err {
		return 0, 0, err
	}
	body, _ := httputil.DumpResponse(resp, true)
	log.Println(string(body))
	json.NewDecoder(resp.Body).Decode(&hereMapResp)
	if "" != hereMapResp.ErrorName {
		return 0, 0, errors.New(hereMapResp.ErrorSubtype)
	}
	return hereMapResp.Response.Route[0].Summary.Distance, hereMapResp.Response.Route[0].Summary.TravelTime, nil
}

func PrepareShortestRoute(points ...[]string) url.Values {
	v := url.Values{}
	v.Set(HereMapAppIdKey, HereMapAppId)
	v.Set(HereMapAppCodeKey, HereMapAppCode)
	v.Set(HereMapModeKey, HereMapRouteMode)

	for index, point := range points {
		v.Add(fmt.Sprintf("waypoint%d", index), strings.Join(point, ","))
	}
	return v
}
