package utils

import (
	"net/url"
	"testing"

	"github.com/stretchr/testify/assert"
	httpmock "gopkg.in/jarcoal/httpmock.v1"
)

func TestPassGetShortestRoute(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	points := [][]string{
		[]string{"1", "2"},
		[]string{"3", "4"},
		[]string{"5", "6"},
	}

	v := PrepareShortestRoute(points...)
	u, _ := url.Parse(HereMapURI)
	u.RawQuery = v.Encode()

	httpmock.RegisterResponder("GET", u.String(),
		httpmock.NewStringResponder(200, `{
			"response": {
				"metaInfo": {
					"timestamp": "2018-05-05T04:00:47Z",
					"mapVersion": "8.30.82.154",
					"moduleVersion": "7.2.201816-20250",
					"interfaceVersion": "2.6.34",
					"availableMapVersion": [
						"8.30.82.154"
					]
				},
				"route": [
					{
						"waypoint": [
							{
								"linkId": "-723430606",
								"mappedPosition": {
									"latitude": 50.0523376,
									"longitude": 8.2179666
								},
								"originalPosition": {
									"latitude": 50.0521999,
									"longitude": 8.2179999
								},
								"type": "stopOver",
								"spot": 1,
								"sideOfStreet": "right",
								"mappedRoadName": "",
								"label": "A66",
								"shapeIndex": 0
							},
							{
								"linkId": "+67198634",
								"mappedPosition": {
									"latitude": 50.0477336,
									"longitude": 8.5550133
								},
								"originalPosition": {
									"latitude": 50.0459999,
									"longitude": 8.5561
								},
								"type": "stopOver",
								"spot": 0.25,
								"sideOfStreet": "neither",
								"mappedRoadName": "Airportring",
								"label": "Airportring - K823",
								"shapeIndex": 178
							},
							{
								"linkId": "+1089077475",
								"mappedPosition": {
									"latitude": 50.0946538,
									"longitude": 8.5286458
								},
								"originalPosition": {
									"latitude": 50.0957,
									"longitude": 8.528
								},
								"type": "stopOver",
								"spot": 0.8607595,
								"sideOfStreet": "neither",
								"mappedRoadName": "",
								"label": "",
								"shapeIndex": 312
							}
						],
						"mode": {
							"type": "fastest",
							"transportModes": [
								"truck"
							],
							"trafficMode": "disabled",
							"feature": []
						},
						"leg": [
							{
								"start": {
									"linkId": "-723430606",
									"mappedPosition": {
										"latitude": 50.0523376,
										"longitude": 8.2179666
									},
									"originalPosition": {
										"latitude": 50.0521999,
										"longitude": 8.2179999
									},
									"type": "stopOver",
									"spot": 1,
									"sideOfStreet": "right",
									"mappedRoadName": "",
									"label": "A66",
									"shapeIndex": 0
								},
								"end": {
									"linkId": "+67198634",
									"mappedPosition": {
										"latitude": 50.0477336,
										"longitude": 8.5550133
									},
									"originalPosition": {
										"latitude": 50.0459999,
										"longitude": 8.5561
									},
									"type": "stopOver",
									"spot": 0.25,
									"sideOfStreet": "neither",
									"mappedRoadName": "Airportring",
									"label": "Airportring - K823",
									"shapeIndex": 178
								},
								"length": 26086,
								"travelTime": 1253,
								"maneuver": [
									{
										"position": {
											"latitude": 50.0523376,
											"longitude": 8.2179666
										},
										"instruction": "Head <span class=\"heading\">east</span> on <span class=\"number\">A66</span>. <span class=\"distance-description\">Go for <span class=\"length\">11.9 km</span>.</span>",
										"travelTime": 533,
										"length": 11855,
										"id": "M1",
										"_type": "PrivateTransportManeuverType"
									},
									{
										"position": {
											"latitude": 50.0555992,
											"longitude": 8.3805084
										},
										"instruction": "Take exit <span class=\"exit\">9</span> toward <span class=\"sign\"><span lang=\"de\">Würzburg</span>/<span lang=\"de\">Basel</span>/<span lang=\"de\">Stuttgart</span></span> onto <span class=\"number\">A3</span>. <span class=\"distance-description\">Go for <span class=\"length\">12.9 km</span>.</span>",
										"travelTime": 572,
										"length": 12882,
										"id": "M2",
										"_type": "PrivateTransportManeuverType"
									},
									{
										"position": {
											"latitude": 50.0450635,
											"longitude": 8.5401857
										},
										"instruction": "Take exit <span class=\"exit\">49</span> toward <span class=\"sign\"><span lang=\"de\">Flughafen Cargo Center</span></span> onto <span class=\"number\">B43</span>. <span class=\"distance-description\">Go for <span class=\"length\">812 m</span>.</span>",
										"travelTime": 61,
										"length": 812,
										"id": "M3",
										"_type": "PrivateTransportManeuverType"
									},
									{
										"position": {
											"latitude": 50.0489795,
											"longitude": 8.5492623
										},
										"instruction": "Take ramp onto <span class=\"next-street\">Querspange-Kelsterbach</span> toward <span class=\"sign\"><span lang=\"de\">Flughafen Cargo Center</span>/<span lang=\"de\">Tor 2-31</span></span>. <span class=\"distance-description\">Go for <span class=\"length\">486 m</span>.</span>",
										"travelTime": 72,
										"length": 486,
										"id": "M4",
										"_type": "PrivateTransportManeuverType"
									},
									{
										"position": {
											"latitude": 50.0477779,
											"longitude": 8.5543585
										},
										"instruction": "Turn <span class=\"direction\">left</span> onto <span class=\"next-street\">Airportring</span> <span class=\"number\">(K823)</span> toward <span class=\"sign\"><span lang=\"de\">Terminal 1+2</span>/<span lang=\"de\">FAC</span>/<span lang=\"de\">Tor 2-21</span></span>. <span class=\"distance-description\">Go for <span class=\"length\">51 m</span>.</span>",
										"travelTime": 15,
										"length": 51,
										"id": "M5",
										"_type": "PrivateTransportManeuverType"
									},
									{
										"position": {
											"latitude": 50.0477336,
											"longitude": 8.5550133
										},
										"instruction": "Arrive at <span class=\"street\">Airportring</span> <span class=\"number\">(K823)</span>.",
										"travelTime": 0,
										"length": 0,
										"id": "M6",
										"_type": "PrivateTransportManeuverType"
									}
								]
							},
							{
								"start": {
									"linkId": "+67198634",
									"mappedPosition": {
										"latitude": 50.0477336,
										"longitude": 8.5550133
									},
									"originalPosition": {
										"latitude": 50.0459999,
										"longitude": 8.5561
									},
									"type": "stopOver",
									"spot": 0.25,
									"sideOfStreet": "neither",
									"mappedRoadName": "Airportring",
									"label": "Airportring - K823",
									"shapeIndex": 178
								},
								"end": {
									"linkId": "+1089077475",
									"mappedPosition": {
										"latitude": 50.0946538,
										"longitude": 8.5286458
									},
									"originalPosition": {
										"latitude": 50.0957,
										"longitude": 8.528
									},
									"type": "stopOver",
									"spot": 0.8607595,
									"sideOfStreet": "neither",
									"mappedRoadName": "",
									"label": "",
									"shapeIndex": 312
								},
								"length": 9883,
								"travelTime": 1126,
								"maneuver": [
									{
										"position": {
											"latitude": 50.0477336,
											"longitude": 8.5550133
										},
										"instruction": "Head <span class=\"heading\">east</span> on <span class=\"street\">Airportring</span> <span class=\"number\">(K823)</span>. <span class=\"distance-description\">Go for <span class=\"length\">194 m</span>.</span>",
										"travelTime": 43,
										"length": 194,
										"id": "M7",
										"_type": "PrivateTransportManeuverType"
									},
									{
										"position": {
											"latitude": 50.0483358,
											"longitude": 8.5575557
										},
										"instruction": "Make a U-Turn onto <span class=\"next-street\">Airportring</span> <span class=\"number\">(K823)</span>. <span class=\"distance-description\">Go for <span class=\"length\">161 m</span>.</span>",
										"travelTime": 45,
										"length": 161,
										"id": "M8",
										"_type": "PrivateTransportManeuverType"
									},
									{
										"position": {
											"latitude": 50.0480354,
											"longitude": 8.5555065
										},
										"instruction": "Turn <span class=\"direction\">right</span> onto <span class=\"next-street\">Querspange-Kelsterbach</span> toward <span class=\"sign\"><span lang=\"de\">A3</span>/<span lang=\"de\">Köln</span>/<span lang=\"de\">Wiesbaden</span>/<span lang=\"de\">Darmstadt</span>/<span lang=\"de\">Mainz</span>/<span lang=\"de\">Rüsselsheim</span>/<span lang=\"de\">Kelsterbach</span>/<span lang=\"de\">B43</span>/<span lang=\"de\">Würzburg</span>/<span lang=\"de\">Kassel</span>/<span lang=\"de\">A5</span>/<span lang=\"de\">Frankfurt</span></span>. <span class=\"distance-description\">Go for <span class=\"length\">2.9 km</span>.</span>",
										"travelTime": 173,
										"length": 2876,
										"id": "M9",
										"_type": "PrivateTransportManeuverType"
									},
									{
										"position": {
											"latitude": 50.0713062,
											"longitude": 8.5443807
										},
										"instruction": "Take ramp onto <span class=\"next-street\">Südumgehung Frankfurt-Höchst</span> <span class=\"number\">(B40)</span> toward <span class=\"sign\"><span lang=\"de\">A66</span>/<span lang=\"de\">F-Sindlingen</span></span>. <span class=\"distance-description\">Go for <span class=\"length\">2.9 km</span>.</span>",
										"travelTime": 140,
										"length": 2879,
										"id": "M10",
										"_type": "PrivateTransportManeuverType"
									},
									{
										"position": {
											"latitude": 50.074482,
											"longitude": 8.5099196
										},
										"instruction": "Take ramp toward <span class=\"sign\"><span lang=\"de\">F-Höchst</span>/<span lang=\"de\">F-Sindlingen</span>/<span lang=\"de\">Hattersheim-Ost</span>/<span lang=\"de\">Industriepark Höchst</span>/<span lang=\"de\">Tor West/Tor Ost</span></span>. <span class=\"distance-description\">Go for <span class=\"length\">363 m</span>.</span>",
										"travelTime": 44,
										"length": 363,
										"id": "M11",
										"_type": "PrivateTransportManeuverType"
									},
									{
										"position": {
											"latitude": 50.0767994,
											"longitude": 8.5067976
										},
										"instruction": "Continue on <span class=\"next-street\">Hoechster Farbenstraße</span>. <span class=\"distance-description\">Go for <span class=\"length\">1.1 km</span>.</span>",
										"travelTime": 141,
										"length": 1110,
										"id": "M12",
										"_type": "PrivateTransportManeuverType"
									},
									{
										"position": {
											"latitude": 50.0850606,
											"longitude": 8.5147476
										},
										"instruction": "Turn <span class=\"direction\">right</span> onto <span class=\"next-street\">Sindlinger Bahnstraße</span>. <span class=\"distance-description\">Go for <span class=\"length\">233 m</span>.</span>",
										"travelTime": 73,
										"length": 233,
										"id": "M13",
										"_type": "PrivateTransportManeuverType"
									},
									{
										"position": {
											"latitude": 50.0830865,
											"longitude": 8.5158741
										},
										"instruction": "Turn <span class=\"direction\">left</span> onto <span class=\"next-street\">Gustavsallee</span>. <span class=\"distance-description\">Go for <span class=\"length\">160 m</span>.</span>",
										"travelTime": 52,
										"length": 160,
										"id": "M14",
										"_type": "PrivateTransportManeuverType"
									},
									{
										"position": {
											"latitude": 50.083698,
											"longitude": 8.5179234
										},
										"instruction": "Continue straight ahead. <span class=\"distance-description\">Go for <span class=\"length\">86 m</span>.</span>",
										"travelTime": 39,
										"length": 86,
										"id": "M15",
										"_type": "PrivateTransportManeuverType"
									},
									{
										"position": {
											"latitude": 50.0841486,
											"longitude": 8.518889
										},
										"instruction": "Turn <span class=\"direction\">left</span>. <span class=\"distance-description\">Go for <span class=\"length\">1.8 km</span>.</span>",
										"travelTime": 376,
										"length": 1821,
										"id": "M16",
										"_type": "PrivateTransportManeuverType"
									},
									{
										"position": {
											"latitude": 50.0946538,
											"longitude": 8.5286458
										},
										"instruction": "Arrive at your destination.",
										"travelTime": 0,
										"length": 0,
										"id": "M17",
										"_type": "PrivateTransportManeuverType"
									}
								]
							}
						],
						"summary": {
							"distance": 35969,
							"trafficTime": 2402,
							"baseTime": 2379,
							"flags": [
								"tollroad",
								"noThroughRoad",
								"tunnel",
								"motorway",
								"builtUpArea",
								"privateRoad"
							],
							"text": "The trip takes <span class=\"length\">36.0 km</span> and <span class=\"time\">40 mins</span>.",
							"travelTime": 2379,
							"_type": "RouteSummaryType"
						}
					}
				],
				"language": "en-us"
			}
		}`))
	distance, travelTime, err := GetShortestRoute(points...)
	assert.Equal(t, float64(35969), distance)
	assert.Equal(t, float64(2379), travelTime)
	assert.Nil(t, err)
}

func TestFailGetShortestRouteByInvalidResponse(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	points := [][]string{
		[]string{"1", "2"},
		[]string{"3", "4"},
		[]string{"5", "6"},
	}

	v := PrepareShortestRoute(points...)
	u, _ := url.Parse(HereMapURI)
	u.RawQuery = v.Encode()

	httpmock.RegisterResponder("GET", u.String(),
		httpmock.NewStringResponder(200, `{
			"_type": "ns2:RoutingServiceErrorType",
			"type": "ApplicationError",
			"subtype": "NoRouteFound",
			"details": "Error is NGEO_ERROR_ROUTE_NO_START_POINT",
			"additionalData": [
				{
					"key": "error_code",
					"value": "NGEO_ERROR_ROUTE_NO_START_POINT"
				}
			],
			"metaInfo": {
				"timestamp": "2018-05-05T04:20:41Z",
				"mapVersion": "8.30.82.154",
				"moduleVersion": "7.2.201816-20250",
				"interfaceVersion": "2.6.34",
				"availableMapVersion": [
					"8.30.82.154"
				]
			}
		}`))
	distance, travelTime, err := GetShortestRoute(points...)
	assert.Equal(t, float64(0), distance)
	assert.Equal(t, float64(0), travelTime)
	assert.EqualError(t, err, "NoRouteFound")
}

func TestPrepareShortestRoute(t *testing.T) {
	points := [][]string{
		[]string{"1", "2"},
		[]string{"3", "4"},
		[]string{"5", "6"},
	}
	actual := PrepareShortestRoute(points...)
	assert.Equal(t, "shortest;car;traffic:disabled", actual.Get("mode"))
	assert.Equal(t, "1,2", actual.Get("waypoint0"))
	assert.Equal(t, "3,4", actual.Get("waypoint1"))
	assert.Equal(t, "5,6", actual.Get("waypoint2"))
}
