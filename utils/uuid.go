package utils

import (
	uuid "github.com/satori/go.uuid"
)

type UuidInterface interface {
	New() string
}

type UUID struct{}

var U UuidInterface

func NewUuidInstace() UuidInterface {
	if nil == U {
		U = new(UUID)
	}
	return U
}

func (*UUID) New() string {
	return uuid.NewV4().String()
}
