package utils

import "github.com/astaxie/beego"

var HereMapAppId = beego.AppConfig.String("hereMapAppId")
var HereMapAppCode = beego.AppConfig.String("hereMapAppCode")
