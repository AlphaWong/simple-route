package utils

const TokenStatusInProgress = "IN_PROGRESS"

const HereMapAppIdKey = "app_id"
const HereMapAppCodeKey = "app_code"
const HereMapModeKey = "mode"

const HereMapRouteMode = "shortest;car;traffic:disabled"

const HereMapURI = "https://route.cit.api.here.com/routing/7.2/calculateroute.json"

const HereMapCacheKey = "cache-response:%s"

const ResponseFailure = "failure"
const ResponseSuccess = "success"
