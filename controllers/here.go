package controllers

import (
	"encoding/json"
	"log"
	"net/http"
	"strings"
	"time"

	"gitlab.com/simple-route/types"
	"gitlab.com/simple-route/utils"

	"github.com/astaxie/beego"
)

type HereController struct {
	beego.Controller
}

// @router /v1/route [post]
func (this *HereController) GetToken() {
	var points [][]string
	var response types.TokenResponse
	u := utils.NewUuidInstace()
	tokenString := u.New()
	response.Token = tokenString

	err := json.NewDecoder(this.Ctx.Request.Body).Decode(&points)
	if nil != err {
		log.Println(err.Error())
		this.Ctx.Output.Status = http.StatusBadRequest
		this.Data["json"] = &types.TokenResponse{
			Error: utils.BadRequestErrorMessage,
		}
		this.ServeJSON()
		this.StopRun()
	}

	c, err := utils.NewCacheConnection()
	if nil != err {
		log.Println(err.Error())
		this.Ctx.Output.Status = http.StatusInternalServerError
		this.Data["json"] = &types.TokenResponse{
			Error: utils.RedisConnectionErrorMessage,
		}
		this.ServeJSON()
		this.StopRun()
	}

	pointsJSON, _ := json.Marshal(points)
	c.Put(tokenString, string(pointsJSON), time.Duration(utils.CacheTimeout)*time.Second)
	p, err := utils.RedisToString(c.Get(tokenString))
	if nil != err {
		log.Println(err.Error())
		this.Ctx.Output.Status = http.StatusBadRequest
		this.Data["json"] = &types.TokenResponse{
			Error: err.Error(),
		}
		this.ServeJSON()
		this.StopRun()
	}

	log.Printf("%v \n", p)
	this.Data["json"] = response
	this.ServeJSON()
}

// @router /v1/route/:token [get]
func (this *HereController) GetShortestRoute() {
	var points [][]string
	tokenString := this.Ctx.Input.Param(":token")
	c, err := utils.NewCacheConnection()
	if nil != err {
		this.Ctx.Output.Status = http.StatusInternalServerError
		this.Data["json"] = &types.ShortestRouteResponse{
			Status: utils.ResponseFailure,
			Error:  err.Error(),
		}
		this.ServeJSON()
		this.StopRun()
	}

	p, err := utils.RedisToString(c.Get(tokenString))
	if nil != err {
		this.Ctx.Output.Status = http.StatusNotFound
		this.Data["json"] = &types.ShortestRouteResponse{
			Status: utils.ResponseFailure,
			Error:  utils.TokenNotFoundErrorMessage,
		}
		this.ServeJSON()
		this.StopRun()
	}

	err = json.NewDecoder(strings.NewReader(p)).Decode(&points)
	if nil != err {
		this.Ctx.Output.Status = http.StatusBadRequest
		this.Data["json"] = &types.ShortestRouteResponse{
			Status: utils.ResponseFailure,
			Error:  err.Error(),
		}
		this.ServeJSON()
		this.StopRun()
	}

	var s *types.ShortestRouteResponse

	s, _ = utils.GetResponseFromCache(tokenString)
	if nil != s {
		this.Data["json"] = s
		this.ServeJSON()
		this.StopRun()
	} else {
		distance, travelTime, err := utils.GetShortestRoute(points...)
		if nil != err {
			this.Ctx.Output.Status = http.StatusBadGateway
			this.Data["json"] = &types.ShortestRouteResponse{
				Status: utils.ResponseFailure,
				Error:  err.Error(),
			}
			this.ServeJSON()
			this.StopRun()
		}

		s := &types.ShortestRouteResponse{
			Status:        utils.ResponseSuccess,
			Path:          points,
			TotalDistance: distance,
			TotalTime:     travelTime,
		}

		utils.SetResponseToCache(tokenString, s)

		this.Data["json"] = s
		this.ServeJSON()
	}
}
