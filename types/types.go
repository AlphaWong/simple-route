package types

import "time"

type (
	TokenResponse struct {
		Token string `json:"token,omitempty"`
		Error string `json:"error,omitempty"`
	}

	ShortestRouteResponse struct {
		Status        string     `json:"status,omitempty"`
		Path          [][]string `json:"path,omitempty"`
		TotalDistance float64    `json:"total_distance,omitempty"`
		TotalTime     float64    `json:"total_time,omitempty"`
		Error         string     `json:"error,omitempty"`
	}

	HereMapResponse struct {
		ErrorType           string `json:"_type"`
		ErrorName           string `json:"type"`
		ErrorSubtype        string `json:"subtype"`
		ErrorDetails        string `json:"details"`
		ErrorAdditionalData []struct {
			Key   string `json:"key"`
			Value string `json:"value"`
		} `json:"additionalData"`
		Response struct {
			MetaInfo struct {
				Timestamp           time.Time `json:"timestamp"`
				MapVersion          string    `json:"mapVersion"`
				ModuleVersion       string    `json:"moduleVersion"`
				InterfaceVersion    string    `json:"interfaceVersion"`
				AvailableMapVersion []string  `json:"availableMapVersion"`
			} `json:"metaInfo"`
			Route []struct {
				Waypoint []struct {
					LinkID         string `json:"linkId"`
					MappedPosition struct {
						Latitude  float64 `json:"latitude"`
						Longitude float64 `json:"longitude"`
					} `json:"mappedPosition"`
					OriginalPosition struct {
						Latitude  float64 `json:"latitude"`
						Longitude float64 `json:"longitude"`
					} `json:"originalPosition"`
					Type           string  `json:"type"`
					Spot           float64 `json:"spot"`
					SideOfStreet   string  `json:"sideOfStreet"`
					MappedRoadName string  `json:"mappedRoadName"`
					Label          string  `json:"label"`
					ShapeIndex     int     `json:"shapeIndex"`
				} `json:"waypoint"`
				Mode struct {
					Type           string        `json:"type"`
					TransportModes []string      `json:"transportModes"`
					TrafficMode    string        `json:"trafficMode"`
					Feature        []interface{} `json:"feature"`
				} `json:"mode"`
				Leg []struct {
					Start struct {
						LinkID         string `json:"linkId"`
						MappedPosition struct {
							Latitude  float64 `json:"latitude"`
							Longitude float64 `json:"longitude"`
						} `json:"mappedPosition"`
						OriginalPosition struct {
							Latitude  float64 `json:"latitude"`
							Longitude float64 `json:"longitude"`
						} `json:"originalPosition"`
						Type           string  `json:"type"`
						Spot           float64 `json:"spot"`
						SideOfStreet   string  `json:"sideOfStreet"`
						MappedRoadName string  `json:"mappedRoadName"`
						Label          string  `json:"label"`
						ShapeIndex     int     `json:"shapeIndex"`
					} `json:"start"`
					End struct {
						LinkID         string `json:"linkId"`
						MappedPosition struct {
							Latitude  float64 `json:"latitude"`
							Longitude float64 `json:"longitude"`
						} `json:"mappedPosition"`
						OriginalPosition struct {
							Latitude  float64 `json:"latitude"`
							Longitude float64 `json:"longitude"`
						} `json:"originalPosition"`
						Type           string  `json:"type"`
						Spot           float64 `json:"spot"`
						SideOfStreet   string  `json:"sideOfStreet"`
						MappedRoadName string  `json:"mappedRoadName"`
						Label          string  `json:"label"`
						ShapeIndex     int     `json:"shapeIndex"`
					} `json:"end"`
					Length     float64 `json:"length"`
					TravelTime float64 `json:"travelTime"`
					Maneuver   []struct {
						Position struct {
							Latitude  float64 `json:"latitude"`
							Longitude float64 `json:"longitude"`
						} `json:"position"`
						Instruction string  `json:"instruction"`
						TravelTime  float64 `json:"travelTime"`
						Length      float64 `json:"length"`
						ID          string  `json:"id"`
						Type        string  `json:"_type"`
					} `json:"maneuver"`
				} `json:"leg"`
				Summary struct {
					Distance    float64  `json:"distance"`
					TrafficTime float64  `json:"trafficTime"`
					BaseTime    float64  `json:"baseTime"`
					Flags       []string `json:"flags"`
					Text        string   `json:"text"`
					TravelTime  float64  `json:"travelTime"`
					Type        string   `json:"_type"`
				} `json:"summary"`
			} `json:"route"`
			Language string `json:"language"`
		} `json:"response"`
	}
)
