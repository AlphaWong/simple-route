# Start by building the application.
FROM golang:1 as builder

ARG PROJECT_DIR=/go/src/gitlab.com/simple-route

RUN apt-get update && apt-get install -y --no-install-recommends upx

WORKDIR ${PROJECT_DIR}

COPY . .

RUN go get -u github.com/golang/dep/cmd/dep && dep ensure -v -vendor-only

RUN  CGO_ENABLE=0 GOOS=linux go build \
 -tags netgo \
 -installsuffix netgo,cgo \
 -v -a \
 -ldflags '-s -w -extldflags "-static"' \
 -o app

FROM gcr.io/distroless/base

ARG PROJECT_DIR=/go/src/gitlab.com/simple-route

WORKDIR ${PROJECT_DIR}
COPY --from=builder ${PROJECT_DIR} .
ENTRYPOINT ["./app"]
EXPOSE 3000

