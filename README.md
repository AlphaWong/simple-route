[![pipeline status](https://gitlab.com/AlphaWong/simple-route/badges/master/pipeline.svg)](https://gitlab.com/AlphaWong/simple-route/commits/master)
[![coverage report](https://codecov.io/gl/AlphaWong/simple-route/branch/master/graph/badge.svg)](https://codecov.io/gl/AlphaWong/simple-route)

# Test
```sh
go test ./...
```

# Minikube init
```sh
minikube start
```

# Change docker image location
```sh
eval $(minikube docker-env)
```

# Init redis
```sh
kubectl create -f ./redis-deployment.yaml
```

# Build Docker image
```sh
docker build . -t route:0.0.1
```

# Start service
```sh
kubectl run route --env="GOPATH=/go" --image=route:0.0.1 --replicas=2 --port=4000 --image-pull-policy=Never -- --host=redis-master
```

# Forward port to host
```sh
kubectl port-forward deployment.apps/route 4000:4000
```

# Run
```sh
docker run --env="GOPATH=/go" --rm -it -p 4000:4000 route:0.0.1
```


